import App from './App'
import Toast from '/wxcomponents/vant/toast/toast.js'

// #ifndef VUE3
import Vue from 'vue'
Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
    ...App
})
app.$mount()
// #endif

let timer = ''
let isSuccess = false

uni.addInterceptor('request', {
	invoke(args) {
		// request 触发前拼接 url 
		// args.url = 'http://192.168.0.125:8000/api/'+args.url
		args.url = 'http://42.192.15.126:8081/api/' + args.url
		isSuccess = true
		if(isSuccess){
			clearInterval(timer)
			timer = setInterval(() => {
				Toast.loading({
					message: '加载中...',
					duration:500,
					forbidClick: true,
				});
			}, 500)
		}
		console.log("start")
	},
	success(args) {
		args.data.code = 1
		if(isSuccess == true){
			clearInterval(timer)
			isSuccess = false
		}
		// clearInterval(timer)
		console.log("end")
		
	},
	fail(err) {
		console.log('interceptor-fail', err)
	},
	complete(res) {
		console.log('interceptor-complete', res)
		// timmer = null
		// clearInterval(timer)
	}
})

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif