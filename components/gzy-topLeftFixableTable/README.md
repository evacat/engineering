实现思路:
1、左侧列冻结:左右用两个table，左侧的table z-index > 右侧的table
2、顶部列冻结:thead的position使用fixed
	但同时如果还需要左侧列冻结，则需要监听水平滑动事件，使得右侧表格head和body部分左右滑动一致

| 属性 | 注释 | 举例 |
| ---- | ---- | ---- |
| tableHead | 表头列表 | [{name: '列1', key: 'column1', width: 80}, {name: '列2', key: 'column2', width: 80}] |
| leftFrozenColumns | 冻结的表头列表，默认为空不需要冻结 | [{name: '列1', key: 'column1', width: 80}] |
| tableHeadFrozen | 表头是否要冻结，默认不需要冻结 | false |
| tableList | 表格数据 | [{column1: 'data1_1', column2: 'data1_2'}, {column1: 'data2_1', column2: 'data2_2'}] |
 
 使用方法：
 ```
 <template>
 	<view class="content" style="height: 200px;">
 		<gzy-topLeftFixableTable
 			:tableHead="tableHead"
 			:tableHeadFrozen="true"
 			:leftFrozenColumns="leftFrozenColumns"
 			:tableList="tableList"></gzy-topLeftFixableTable>
 	</view>
 </template>
 
 <script>
 	export default {
 		data() {
 			return {
 				tableList: [
 					{column1: 'data1_1', column2: 'data1_2', column3: 'data1_3', column4: 'data1_4', column5: 'data1_5', column6: 'data1_6', column7: 'data1_7', column8: 'data1_8', column9: 'data1_9'},
 					{column1: 'data2_1', column2: 'data2_2', column3: 'data2_3', column4: 'data2_4', column5: 'data2_5', column6: 'data2_6', column7: 'data2_7', column8: 'data2_8', column9: 'data2_9'},
 					{column1: 'data3_1', column2: 'data3_2', column3: 'data3_3', column4: 'data3_4', column5: 'data3_5', column6: 'data3_6', column7: 'data3_7', column8: 'data3_8', column9: 'data3_9'},
 					{column1: 'data4_1', column2: 'data4_2', column3: 'data4_3', column4: 'data4_4', column5: 'data4_5', column6: 'data4_6', column7: 'data4_7', column8: 'data4_8', column9: 'data4_9'},
 					{column1: 'data5_1', column2: 'data5_2', column3: 'data5_3', column4: 'data5_4', column5: 'data5_5', column6: 'data5_6', column7: 'data5_7', column8: 'data5_8', column9: 'data5_9'},
 					{column1: 'data6_1', column2: 'data6_2', column3: 'data6_3', column4: 'data6_4', column5: 'data6_5', column6: 'data6_6', column7: 'data6_7', column8: 'data6_8', column9: 'data6_9'},
 					{column1: 'data7_1', column2: 'data7_2', column3: 'data7_3', column4: 'data7_4', column5: 'data7_5', column6: 'data7_6', column7: 'data7_7', column8: 'data7_8', column9: 'data7_9'},
 					{column1: 'data8_1', column2: 'data8_2', column3: 'data8_3', column4: 'data8_4', column5: 'data8_5', column6: 'data8_6', column7: 'data8_7', column8: 'data8_8', column9: 'data8_9'},
 					{column1: 'data9_1', column2: 'data9_2', column3: 'data9_3', column4: 'data9_4', column5: 'data9_5', column6: 'data9_6', column7: 'data9_7', column8: 'data9_8', column9: 'data9_9'},
 					{column1: 'data10_1', column2: 'data10_2', column3: 'data10_3', column4: 'data10_4', column5: 'data10_5', column6: 'data10_6', column7: 'data10_7', column8: 'data10_8', column9: 'data10_9'},
 					{column1: 'data11_1', column2: 'data11_2', column3: 'data11_3', column4: 'data11_4', column5: 'data11_5', column6: 'data11_6', column7: 'data11_7', column8: 'data11_8', column9: 'data11_9'},
 					{column1: 'data12_1', column2: 'data12_2', column3: 'data12_3', column4: 'data12_4', column5: 'data12_5', column6: 'data12_6', column7: 'data12_7', column8: 'data12_8', column9: 'data12_9'},
 					{column1: 'data13_1', column2: 'data13_2', column3: 'data13_3', column4: 'data13_4', column5: 'data13_5', column6: 'data13_6', column7: 'data13_7', column8: 'data13_8', column9: 'data13_9'},
 					{column1: 'data14_1', column2: 'data14_2', column3: 'data14_3', column4: 'data14_4', column5: 'data14_5', column6: 'data14_6', column7: 'data14_7', column8: 'data14_8', column9: 'data14_9'},
 					{column1: 'data15_1', column2: 'data15_2', column3: 'data15_3', column4: 'data15_4', column5: 'data15_5', column6: 'data15_6', column7: 'data15_7', column8: 'data15_8', column9: 'data15_9'},
 					{column1: 'data16_1', column2: 'data16_2', column3: 'data16_3', column4: 'data16_4', column5: 'data16_5', column6: 'data16_6', column7: 'data16_7', column8: 'data16_8', column9: 'data16_9'},
 					{column1: 'data17_1', column2: 'data17_2', column3: 'data17_3', column4: 'data17_4', column5: 'data17_5', column6: 'data17_6', column7: 'data17_7', column8: 'data17_8', column9: 'data17_9'},
 					{column1: 'data18_1', column2: 'data18_2', column3: 'data18_3', column4: 'data18_4', column5: 'data18_5', column6: 'data18_6', column7: 'data18_7', column8: 'data18_8', column9: 'data18_9'},
 					{column1: 'data19_1', column2: 'data19_2', column3: 'data19_3', column4: 'data19_4', column5: 'data19_5', column6: 'data19_6', column7: 'data19_7', column8: 'data19_8', column9: 'data19_9'},
 					{column1: 'data1_1', column2: 'data1_2', column3: 'data1_3', column4: 'data1_4', column5: 'data1_5', column6: 'data1_6', column7: 'data1_7', column8: 'data1_8', column9: 'data1_9'},
 					{column1: 'data2_1', column2: 'data2_2', column3: 'data2_3', column4: 'data2_4', column5: 'data2_5', column6: 'data2_6', column7: 'data2_7', column8: 'data2_8', column9: 'data2_9'},
 					{column1: 'data3_1', column2: 'data3_2', column3: 'data3_3', column4: 'data3_4', column5: 'data3_5', column6: 'data3_6', column7: 'data3_7', column8: 'data3_8', column9: 'data3_9'},
 					{column1: 'data4_1', column2: 'data4_2', column3: 'data4_3', column4: 'data4_4', column5: 'data4_5', column6: 'data4_6', column7: 'data4_7', column8: 'data4_8', column9: 'data4_9'},
 					{column1: 'data5_1', column2: 'data5_2', column3: 'data5_3', column4: 'data5_4', column5: 'data5_5', column6: 'data5_6', column7: 'data5_7', column8: 'data5_8', column9: 'data5_9'},
 					{column1: 'data6_1', column2: 'data6_2', column3: 'data6_3', column4: 'data6_4', column5: 'data6_5', column6: 'data6_6', column7: 'data6_7', column8: 'data6_8', column9: 'data6_9'},
 					{column1: 'data7_1', column2: 'data7_2', column3: 'data7_3', column4: 'data7_4', column5: 'data7_5', column6: 'data7_6', column7: 'data7_7', column8: 'data7_8', column9: 'data7_9'},
 					{column1: 'data8_1', column2: 'data8_2', column3: 'data8_3', column4: 'data8_4', column5: 'data8_5', column6: 'data8_6', column7: 'data8_7', column8: 'data8_8', column9: 'data8_9'},
 					{column1: 'data9_1', column2: 'data9_2', column3: 'data9_3', column4: 'data9_4', column5: 'data9_5', column6: 'data9_6', column7: 'data9_7', column8: 'data9_8', column9: 'data9_9'},
 					{column1: 'data10_1', column2: 'data10_2', column3: 'data10_3', column4: 'data10_4', column5: 'data10_5', column6: 'data10_6', column7: 'data10_7', column8: 'data10_8', column9: 'data10_9'},
 					{column1: 'data11_1', column2: 'data11_2', column3: 'data11_3', column4: 'data11_4', column5: 'data11_5', column6: 'data11_6', column7: 'data11_7', column8: 'data11_8', column9: 'data11_9'},
 					{column1: 'data12_1', column2: 'data12_2', column3: 'data12_3', column4: 'data12_4', column5: 'data12_5', column6: 'data12_6', column7: 'data12_7', column8: 'data12_8', column9: 'data12_9'},
 					{column1: 'data13_1', column2: 'data13_2', column3: 'data13_3', column4: 'data13_4', column5: 'data13_5', column6: 'data13_6', column7: 'data13_7', column8: 'data13_8', column9: 'data13_9'},
 					{column1: 'data14_1', column2: 'data14_2', column3: 'data14_3', column4: 'data14_4', column5: 'data14_5', column6: 'data14_6', column7: 'data14_7', column8: 'data14_8', column9: 'data14_9'},
 					{column1: 'data15_1', column2: 'data15_2', column3: 'data15_3', column4: 'data15_4', column5: 'data15_5', column6: 'data15_6', column7: 'data15_7', column8: 'data15_8', column9: 'data15_9'},
 					{column1: 'data16_1', column2: 'data16_2', column3: 'data16_3', column4: 'data16_4', column5: 'data16_5', column6: 'data16_6', column7: 'data16_7', column8: 'data16_8', column9: 'data16_9'},
 					{column1: 'data17_1', column2: 'data17_2', column3: 'data17_3', column4: 'data17_4', column5: 'data17_5', column6: 'data17_6', column7: 'data17_7', column8: 'data17_8', column9: 'data17_9'},
 					{column1: 'data18_1', column2: 'data18_2', column3: 'data18_3', column4: 'data18_4', column5: 'data18_5', column6: 'data18_6', column7: 'data18_7', column8: 'data18_8', column9: 'data18_9'},
 					{column1: 'data19_1', column2: 'data19_2', column3: 'data19_3', column4: 'data19_4', column5: 'data19_5', column6: 'data19_6', column7: 'data19_7', column8: 'data19_8', column9: 'data19_9'},
 				],
 				leftFrozenColumns: [{name: '列1', key: 'column1', width: 80}, {name: '列2', key: 'column2', width: 80}, {name: '列3', key: 'column3', width: 80}],
 				tableHead: [
 					{name: '列1', key: 'column1', width: 80}, {name: '列2', key: 'column2', width: 80}, {name: '列3', key: 'column3', width: 80},
 					{name: '列4', key: 'column4', width: 80}, {name: '列5', key: 'column5', width: 80}, {name: '列6', key: 'column6', width: 80},
 					{name: '列7', key: 'column7', width: 80}, {name: '列8', key: 'column8', width: 80}, {name: '列9', key: 'column9', width: 80},
 				]
 			}
 		},
 		onLoad() {
 		},
 		methods: {
 		}
 	}
 </script>
 ```